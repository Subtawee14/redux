import { SEARCH } from "../actions/SearchAction";

export const initialSearch = "";

export const searchReducer = (state = initialSearch, action) => {
  switch (action.type) {
    case SEARCH:
      return action.payLoad;
    default:
      return state;
  }
};
