import { combineReducers } from "redux";
import { searchReducer } from "./SearchReducer";
import { filterReducer } from "./FilterReducer";
import { todoReducer } from "./TodoReducer";
import { authReducer } from "./AuthReducer";
export const rootReducer = combineReducers({
  todo: todoReducer,
  filter: filterReducer,
  search: searchReducer,
  login: authReducer
});
