import {
  DONE_TODO,
  FETCH_TODO_BEGIN,
  FETCH_TODO_SUCCESS,
  FETCH_TODO_ERROR,
  fetchTodoBegin,
  fetchTodoSuccess,
  fetchTodoError
} from "../actions/TodoAction";

// const initialTodos = [];

export const fetchTodos = (id) => {
  return (dispatch) => {
    dispatch(fetchTodoBegin())
    return fetch("http://jsonplaceholder.typicode.com/todos?userId="+id)
      .then((res) => res.json())
      .then((data) => {
        dispatch(fetchTodoSuccess(data));
      })
      .catch((err) => dispatch(fetchTodoError(err)));
  };
};

const initialState = {
  todo: [],
  loading: false,
  err: ""
};

export const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case DONE_TODO:
      let newArray = [...state.todo];
      newArray.map((value) => {
        if (value.id == action.payLoad) {
          return (value.completed = true);
        }
      });

      return {
        todo: newArray,
        loading: false
      };

    case FETCH_TODO_BEGIN:
      return {
        ...state,
        loading: true
      };
    case FETCH_TODO_SUCCESS:
      return {
        todo: action.payLoad,
        loading: false
      };
    case FETCH_TODO_ERROR:
      return {
        ...state,
        loading: false,
        err: action.payLoad
      };
    case "SET_TODO":
      console.log(action.payLoad);
      return action.payLoad;
    default:
      return state;
  }
};
