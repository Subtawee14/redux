import { LOGIN, LOGOUT } from "../actions/AuthAction";

export const initialStatus = true;

export const authReducer = (state = initialStatus, action) => {
  switch (action.type) {
    case LOGIN:
      return state = true;
    case LOGOUT:
      return state = false;
    default:
      return state;
  }
};
