import React from "react";
import { Route, Redirect } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
const PrivateRoute = (props) => {
  const { path, component: Component, isLogin } = props;
  return (
    <Route
      path={path}
      render={(props) => {
        if (isLogin === true) {
          return <Component {...props} />;
        } else {
          return <Redirect to="/login" />;
        }
      }}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    isLogin: state.login
  };
};

export default connect(mapStateToProps)(PrivateRoute);
