import React from "react";
import { Layout, Menu, Icon, Row, Col, Button } from "antd";
import User from "./User";
import { Route } from "react-router-dom";
import PrivateRoute from "./PrivateRoute";
import Login from "./Login";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import UserTodo from "./UserTodo";
import { logout } from "../actions/AuthAction";
const { Header, Content, Footer } = Layout;

const styles = {
  footer: {
    color: "white",
    backgroundColor: "#001529",
    textAlign: "center"
  }
};
const Main = (props) => {
  const { logout, isLogin } = props;

  console.log(window.location.pathname)
  if (window.location.pathname == "/login" && isLogin == true) {
    window.location.href = '/users'
  }
  return (
    <Layout className="layout" style={{ height: "100vh" }}>
      <Header>
        <div className="logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={["2"]}
          style={{ lineHeight: "64px" }}
        >
          <Menu.Item key="2">User</Menu.Item>
          {isLogin ? (
            <Button
              style={{ float: "right", margin: "15px" }}
              type="danger"
              onClick={() => logout()}
            >
              Log out
            </Button>
          ) : (
            <div></div>
          )}
        </Menu>
      </Header>
      <Content style={{ padding: "50px 50px 0 50px" }}>
        <div
          style={{
            background: "#fff",
            padding: 24,
            minHeight: 280,
            height: "100%"
          }}
        >
          <Route exact path="/" component={Login} />
          <Route path="/login" component={Login} />
          <PrivateRoute exact path="/users" component={User} />
          <PrivateRoute path="/user/:user_id/todo" component={UserTodo} />
        </div>
      </Content>
      <Footer style={styles.footer}>Ant Design ©2018 Created by Ant UED</Footer>
    </Layout>
  );
};
const mapStateToProps = (state) => {
  return {
    isLogin: state.login
  };
};
const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ logout }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);
