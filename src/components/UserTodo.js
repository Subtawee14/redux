import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Table, Tag } from "antd";
import { Row, Spin, Col, notification, Input, Icon } from "antd";
import { Avatar, Button } from "antd";
import { Select } from "antd";
import { useDispatch, connect } from "react-redux";
import { done_todo, setTodo } from "../actions/TodoAction";
import { filterAction } from "../actions/FilterAction";
import { fetchTodos } from "../reducer/TodoReducer";
import { searchAction } from "../actions/SearchAction";
import { bindActionCreators } from "redux";
const { Option } = Select;
const { Search } = Input;

const UserTodo = (props) => {
  const [user, setUser] = useState();
  const filter = useSelector((state) => state.filter);
  // const state = useSelector((state) => state.todo);
  // const {  } = state;

  const { name, searchAction, fetchTodos, done_todo, filterAction } = props;
  const { todo, loading, err } = props.todo;

  const handleChange = (value) => {
    console.log(`selected ${value}`);
    if (value == "1") {
      filterAction(true);
    } else if (value == "2") {
      filterAction(false);
    } else {
      filterAction("all");
    }
  };

  useEffect(() => {
    fetchTodos(props.match.params.user_id);
    fetchUser();
  }, []);

  const columns = [
    {
      title: "ID",
      dataIndex: "id",
      key: "id"
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "title"
    },
    {
      title: "Status",
      dataIndex: "completed",
      key: "completed",
      render: (status) => {
        return status ? (
          <Tag color="green">Done</Tag>
        ) : (
          <Tag color="red">Waiting</Tag>
        );
      }
    },
    {
      title: "Action",
      key: "id",
      dataIndex: "completed",
      render: (status, object) => {
        if (status == false) {
          return (
            <Button
              type="primary"
              onClick={() => {
                check(object.id);
              }}
            >
              Done
            </Button>
          );
        } else {
          return <div>Done</div>;
        }
      }
    }
  ];

  const check = (id) => {
    done_todo(id);
  };

  const showTodo = () => {
    if (filter == "all") {
      return todo;
    } else {
      return todo.filter((value) => {
        return value.completed == filter;
      });
    }
  };

  const filterName = () => {
    return showTodo().filter((value) => {
      return value.title.toLowerCase().match(name.toLowerCase());
    });
  };

  const renderTable = () => {
    return <Table columns={columns} dataSource={filterName()} />;
  };
  const fetchUser = () => {
    fetch(
      "https://jsonplaceholder.typicode.com/users/" + props.match.params.user_id
    )
      .then((response) => response.json())
      .then((data) => {
        setUser(data);
      });
  };
  const renderAvatar = () => {
    if (user) {
      return (
        <Col span={6}>
          <Row>
            <Avatar size={64} icon="user" />
            <h3>{user.name}</h3>
            <h4>{user.email}</h4>
          </Row>
          <Row>
            <h4>Address</h4>
            <p>{user.address.street}</p>
          </Row>
        </Col>
      );
    } else {
      return (
        <Col span={6}>
          <Avatar size={64} icon="user" />
          <h3></h3>
        </Col>
      );
    }
  };

  return (
    <Row>
      {renderAvatar()}
      <Col span={18}>
        <Row>
          <Col>
            <h2>Todo List</h2>
            <Search
              placeholder="input search text"
              onChange={(e) => searchAction(e.target.value)}
              style={{ width: 200 }}
            />
            <Select
              defaultValue="All"
              style={{ width: 120 }}
              onChange={handleChange}
            >
              <Option value="0">All</Option>
              <Option value="1">Done</Option>
              <Option value="2">Waiting</Option>
            </Select>
          </Col>
        </Row>
        <Spin spinning={loading}>{renderTable()}</Spin>
      </Col>
    </Row>
  );
};

const mapStateToProps = (state) => {
  return {
    name: state.search,
    todo: state.todo
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(
    { searchAction, fetchTodos, done_todo, filterAction },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(UserTodo);
