import React from "react";
import { Input, Tooltip, Icon, Button } from "antd";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { login } from "../actions/AuthAction";
const Login = (props) => {
  const { login } = props;
  const styles = {
    form: {
      width: "300px"
    }
  };

//   if(login){
//     window.location.href = "/users";
//   }

  return (
    <div style={styles.form}>
      <h1>Login</h1>
      <Input
        style={{ marginBottom: "10px" }}
        placeholder="Enter your username"
        prefix={<Icon type="user" style={{ color: "rgba(0,0,0,.25)" }} />}
        suffix={
          <Tooltip title="Extra information">
            <Icon type="info-circle" style={{ color: "rgba(0,0,0,.45)" }} />
          </Tooltip>
        }
      />
      <Input.Password
        style={{ marginBottom: "10px" }}
        placeholder="input password"
      />
      <div style={{ margin: "10px" }}>
        <Button
          type="danger"
          onClick={() => {
            login();
           
          }}
        >
          Log in
        </Button>
      </div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ login }, dispatch);
};

export default connect(null, mapDispatchToProps)(Login);
