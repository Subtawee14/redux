import React, { useState, useEffect } from "react";
import { List, message, Avatar, Spin } from "antd";
import { Input } from "antd";
import { connect } from "react-redux";
import { searchAction } from "../actions/SearchAction";
import { bindActionCreators } from "redux";
const { Search } = Input;

const User = (props) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const { name, searchAction } = props;

  useEffect(() => {
    fetchData();
  }, [name]);

  const fetchData = () => {
    setLoading(true);

    if (name) {
      let newArray = data.filter((value) => {
        return value.name.toLowerCase().match(name.toLowerCase());
      });
      setData(newArray);
    } else {
      fetch("https://jsonplaceholder.typicode.com/users")
        .then((response) => response.json())
        .then((data) => {
          setData(data);
        });
    }

    setLoading(false);
  };

  return (
    <div className="demo-infinite-container">
      <Spin spinning={loading} />
      <Search
        placeholder="input search text"
        onChange={(e) => searchAction(e.target.value)}
        style={{ width: 200 }}
      />

      <List
        dataSource={data}
        renderItem={(item) => (
          <List.Item key={item.id}>
            <List.Item.Meta
              avatar={
                <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
              }
              title={<a href="https://ant.design">{item.name.last}</a>}
              description={item.name}
            />
            <div>
              <a href={"/user/" + item.id + "/todo"}>Todo</a>
            </div>
            
          </List.Item>
        )}
      ></List>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    name: state.search
  };
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ searchAction }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(User);
