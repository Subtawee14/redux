export const DONE_TODO = "DONE_TODO";
export const FETCH_TODO_BEGIN = "FETCH_TODO_BEGIN";
export const FETCH_TODO_SUCCESS = "FETCH_TODO_SUCCESS";
export const FETCH_TODO_ERROR = "ERROR";

export const done_todo = (id) => {
  return {
    type: DONE_TODO,
    payLoad: id
  };
};

export const fetchTodoBegin = () => {
  return {
    type: FETCH_TODO_BEGIN
  };
};

export const fetchTodoSuccess = (data) => {
  return {
    type: FETCH_TODO_SUCCESS,
    payLoad : data
  };
};

export const fetchTodoError = (error) => {
  return {
    type: FETCH_TODO_BEGIN,
    payLoad : error
  };
};

export const setTodo = (data) => {
  return {
    type: "SET_TODO",
    payLoad: data
  };
};
