export const SEARCH = "SEARCH";
export const searchAction = (name) => {
    return {
      type: SEARCH,
      payLoad : name
    };
  };